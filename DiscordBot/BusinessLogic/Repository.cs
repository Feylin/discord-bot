﻿using System;
using BusinessLogic.Properties;

namespace BusinessLogic
{
    public class Repository
    {
        private readonly Random _rng = new Random();

        public string GetRandomInsult()
        {
            int currentLine = 1;
            string pick = null;

            var insultsText = Resources.Insults.Split(new[] {Environment.NewLine}, StringSplitOptions.None);

            foreach (string line in insultsText)
            {
                if (_rng.Next(currentLine) == 0)
                {
                    pick = line;
                }
                ++currentLine;
            }
            return pick;
        }
    }
}
