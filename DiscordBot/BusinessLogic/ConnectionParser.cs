﻿using System.Data;
using System.Data.SqlClient;
using DataAccess;

namespace BusinessLogic
{
    public class ConnectionParser
    {
        public string BuildConnectionString(string server = "DESKTOP-KQ7NBVL", string user = "", string pw = "", bool integratedSecurity = true, string db = "DiscordBotBase")
        {
            var sqlConString = new SqlConnectionStringBuilder
            {
                DataSource = server,
                UserID = user,
                Password = pw,
                IntegratedSecurity = integratedSecurity,
                ConnectTimeout = 9,
                InitialCatalog = db
            };

            return sqlConString.ConnectionString;
        }

        public bool TestDatabaseConnection(string connectionString)
        {
            return ConnectionHandler.Instance.TestDatabaseConnection(connectionString);
        }

        public DataTable FillDataTableFromDatabaseTable(string connectionString, string tableName)
        {
            return ConnectionHandler.Instance.FillDataTableFromDatabaseTable(connectionString, tableName);
        }
    }
}
