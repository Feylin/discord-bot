﻿using System.Data;
using System.Data.SqlClient;
using System.Windows.Controls;

namespace DataAccess
{
    public class DiscordDatabaseDependency
    {
        private SqlConnection _sqlConnection;
        private SqlCommand _sqlCommand;
        private SqlDependency _sqlDependency;
        private DataTable _dataTable;
        private string _connectionString;
        private readonly DataGrid _dataGridToRefresh;

        public DiscordDatabaseDependency(DataGrid grdDiscordData)
        {
            // Implement own INotifyProperty would have been prefered.
            // But at least it works, although I have to reference a WPF Usercontrol in my DataAccess project.
            // Which is obviously not ideal.
            _dataGridToRefresh = grdDiscordData;
        }

        public void StartSqlDependency(string connectionString)
        {
            SqlDependency.Start(connectionString);
        }

        public void CreateDatabaseDependency(string connectionString, string tableName)
        {
            // Select * does not work, sqlNotificationEventArgs type results in constant updates.
            using (_sqlConnection = new SqlConnection(connectionString))
            using (_sqlCommand = new SqlCommand($"SELECT Id, Name, Count FROM dbo.{tableName}", _sqlConnection))
            {
                _connectionString = connectionString;

                // Create dependency and associate it with the SqlCommand.
                _sqlDependency = new SqlDependency(_sqlCommand);

                // Subscribe to the SqlDependency event.
                _sqlDependency.OnChange += SqlDependencyOnOnChange;

                _dataTable = new DataTable(tableName);
            }
        }

        private void SqlDependencyOnOnChange(object sender, SqlNotificationEventArgs sqlNotificationEventArgs)
        {
            SqlDependency dependency = sender as SqlDependency;

            // Notices are only a one shot deal
            // so remove the existing one so a new 
            // one can be added
            if (dependency != null)
                dependency.OnChange -= SqlDependencyOnOnChange;

            GetData();

            // Updating GUI is not allowed from a different thread.
            _dataGridToRefresh.Dispatcher.Invoke(() => _dataGridToRefresh.Items.Refresh());
        }

        public DataTable GetData()
        {
            _dataTable.Clear();

            // Make sure the command object does not already have
            // a notification object associated with it.
            _sqlCommand.Notification = null;

            _sqlDependency = new SqlDependency(_sqlCommand);
            _sqlDependency.OnChange += SqlDependencyOnOnChange;

            using (_sqlConnection = new SqlConnection(_connectionString))
            using (_sqlCommand.Connection = _sqlConnection)
            using (SqlDataAdapter adapter = new SqlDataAdapter(_sqlCommand))
            {
                adapter.Fill(_dataTable);

                return _dataTable;
            }
        }

        public void TerminateDependency(string connectionString)
        {
            SqlDependency.Stop(connectionString);
        }
    }
}
