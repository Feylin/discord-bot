﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess
{
    public class ConnectionHandler
    {
        private static readonly Lazy<ConnectionHandler> Lazy = new Lazy<ConnectionHandler>(() => new ConnectionHandler());

        public static ConnectionHandler Instance = Lazy.Value;

        private ConnectionHandler()
        {
        }

        public bool TestDatabaseConnection(string connectionString)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                {
                    sqlConn.Open();
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public DataTable FillDataTableFromDatabaseTable(string connectionString, string tableName)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connectionString))
                using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter($"SELECT * FROM [{tableName}]", sqlConn))
                {
                    DataTable dataTable = new DataTable();

                    sqlDataAdapter.Fill(dataTable);  // connection opened by the adapter
                    dataTable.TableName = tableName;

                    return dataTable;
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
