﻿using System;
using System.IO;
using BusinessLogic;

namespace BotLogic
{
    // NOTE: In order to launch WCF Test Client for testing this service, please select BotService.svc or BotService.svc.cs at the Solution Explorer
    // and start debugging.
    public class Service1 : IBotService
    {
        private readonly Repository _repo = new Repository();

        public string SayHello(string name)
        {
            return $"Beautiful weather we are having, isn't that right {name}";
        }

        public string GetRandomInsult()
        {
            return _repo.GetRandomInsult();
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
                throw new ArgumentNullException(nameof(composite));
            if (composite.BoolValue)
                composite.StringValue += "Suffix";
            return composite;
        }
    }
}
