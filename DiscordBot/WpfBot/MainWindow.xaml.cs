﻿using System;
using System.Data;
using System.Windows;
using BusinessLogic;
using DataAccess;

namespace WpfBot
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ConnectionParser _connectionParser = new ConnectionParser();
        private readonly DiscordDatabaseDependency _discordDependency;
        private string _connectionString;
        // Will probably remove this.
        private bool _isConnectedToDatabase;

        public MainWindow()
        {
            InitializeComponent();

            _discordDependency = new DiscordDatabaseDependency(GrdDiscordData);

            FillDataGrid();
        }

        private void FillDataGrid()
        {
            _connectionString = _connectionParser.BuildConnectionString();
            const string tableName = "IssuedCommandsCount";

            try
            {
                _discordDependency.StartSqlDependency(_connectionString);
                _discordDependency.CreateDatabaseDependency(_connectionString, tableName);

                DataTable dataTable = _discordDependency.GetData();
                GrdDiscordData.ItemsSource = dataTable.DefaultView;
                GrdDiscordData.AutoGenerateColumns = true;
                GrdDiscordData.CanUserAddRows = false;

                _isConnectedToDatabase = true;
            }
            catch
            {
                _isConnectedToDatabase = false;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _discordDependency.TerminateDependency(_connectionString);
        }
    }
}
