﻿using System;
using BusinessLogic;

namespace TestConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            ConnectionParser c = new ConnectionParser();

            Console.WriteLine(c.TestDatabaseConnection(c.BuildConnectionString()));
        }
    }
}
