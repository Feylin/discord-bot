﻿using System;
using System.Linq;
using ConsoleBot.DiscordBotServiceReference;
using Discord;
using Discord.Commands;

namespace ConsoleBot
{
    public class DiscordBot
    {
        // Should load it from someplace else and not store it here.
        private const string Token = "Mjk1ODYzMTg2NzE5MTEzMjE2.C7p4nA.u-xfsY87UXBrNRVCJ5A69jO0v9g";
        private BotServiceClient _botService;
        private readonly CommandService _commands;

        // Refactor/mve the bot class to an appropriate class/project.
        public DiscordBot()
        {
            _botService = InstantiateBotService;

            var client = new DiscordClient(builder =>
            {
                builder.LogLevel = LogSeverity.Info;
                builder.LogHandler = Log;
            });

            client.UsingCommands(builder =>
            {
                builder.PrefixChar = '!';
                builder.AllowMentionPrefix = true;
            });

            _commands = client.GetService<CommandService>();

            // Refactor the bot commands to a different class.
            RegisterSubscribeToAutoChannel();
            RegisterUnsubscribeToAutoChannel();
            RegisterHelloCommand();
            RegisterInsultCommand();

            // Run on different thread/task?
            // while(true)?
            client.ExecuteAndWait(async () =>
            {
                await client.Connect(Token, TokenType.Bot).ConfigureAwait(false);
            });
        }

        private void Log(object sender, LogMessageEventArgs e) => Console.WriteLine(e.Message);

        private BotServiceClient InstantiateBotService => _botService ?? (_botService = new BotServiceClient());

        private void RegisterSubscribeToAutoChannel()
        {
            // Figure out if the bot can 'force' users to join a channel, and if there is some way
            // to add custom properties or the like to users on Discord.
            // Create task that runs every so often that moves users to the correct game channel,
            // if it exists.
            throw new NotImplementedException();
        }

        private void RegisterUnsubscribeToAutoChannel()
        {
            // Unsubscribe the user from the auto channel task.
            throw new NotImplementedException();
        }

        private void RegisterHelloCommand()
        {
            _commands.CreateCommand("hello")
                .Do(async c =>
                {
                    string user = c.User.Name;
                    string message = await _botService.SayHelloAsync(user).ConfigureAwait(false);

                    await c.Channel.SendMessage(message).ConfigureAwait(false);
                });
        }

        private void RegisterInsultCommand()
        {
            _commands.CreateCommand("insult")
                .Description("Insults a user.")
                .Parameter("InsultedUser")
                .Do(async c =>
                {
                    string insultedUser = c.GetArg("InsultedUser");

                    if (c.Server.FindUsers(insultedUser).First() == null)
                        await c.Channel.SendMessage($"{insultedUser} does not exists.").ConfigureAwait(false);

                    if (c.Server.FindUsers(insultedUser).First().Status == UserStatus.Offline)
                        await c.Channel.SendMessage($"{insultedUser} is offline.").ConfigureAwait(false);

                    string insult = await _botService.GetRandomInsultAsync().ConfigureAwait(false);

                    await c.Channel.SendMessage($"{c.Server.FindUsers(insultedUser).First().Mention} {insult}").ConfigureAwait(false);
                });
        }
    }
}
