﻿namespace ConsoleBot
{
    internal class Program
    {
        private static DiscordBot _bot;

        private static void Main(string[] args)
        {
            _bot = InstantiateBot;
        }

        private static DiscordBot InstantiateBot => _bot ?? (_bot = new DiscordBot());
    }
}
